# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
from settings import conf_dic 



PORT = conf_dic["appport"]
URL_INICIAL = "http://ensaiosclinicos.gov.br:%s/static/index.html" % PORT 

class retorna_pendings(tornado.web.RequestHandler):
    def get(self):
        #self.render("../retorna-pendings.html")
        self.redirect(URL_INICIAL, permanent=False, status=None)
