# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import os
import MySQLdb
import datetime
import sqlite3
from datetime import date
from settings import conf_dic 

def diffDate(HOJE, DATA_LIMITE):
    d1 = HOJE
    d2 = DATA_LIMITE
    delta = d2-d1
    r = delta.days
    return r


dic_meses = {1: "jan",
             2: "fev",
             3: "mar",
             4: "abr",
             5: "mai",
             6: "jun",
             7: "jul",
             8: "ago",
             9: "set",
             10: "out",
             11: "nov",
             12: "dez"
            }

def retorna_dic_report(ano_report):
        print ">>> report ensaios ano = %s" % ano_report
        ano_limite_superior = ano_report + 1

 
        query_report = 'select created from repository_clinicaltrial where created >= "%s-01-01" and created < "%s-01-01";' % (ano_report,
                                                                                                                              ano_limite_superior)
        print query_report

        db = MySQLdb.connect(host= conf_dic["rebecdbhost"],
                      user= conf_dic["rebecdbuser"],
                      passwd= conf_dic["rebecdbpass"],
                      db= conf_dic["rebecdb"])
        cur = db.cursor()


        cur.execute(query_report)
        
        dic_result = {}
        for row in cur.fetchall():
                try:
                   dic_result[dic_meses[row[0].month]] = dic_result[dic_meses[row[0].month]] + 1
                except:
                   if row[0].month == 12:
                       print row[0]
                   dic_result[dic_meses[row[0].month]] = 1
                #print row[0].month
        cur.close()
        saida =  "%s" % dic_result
        saida = saida.replace("'", '"')
        print ">>> fim  report ensaios" 
        return saida


class ReportCreatedTrials(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))

        ano_report = int(self.get_argument('ano', '47'))
        
        saida = retorna_dic_report(ano_report) 

        self.write(saida)


if __name__ == "__main__":
    retorna_dic_report(2014)
