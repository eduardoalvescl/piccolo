# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import os
import datetime
import sqlite3
from datetime import date
from settings import *


def insere_data_submissao(trial_id, db_file):
    print ">>insere_data_submissao(%s,%s)" % (trial_id, db_file)
    hoje = datetime.datetime.today()
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    sql_update = 'update submited_info set data_sub = "%s" where trial_id = %s;' % (hoje, trial_id)
    sql_insert = 'insert into submited_info values (%s, "%s");' % (trial_id, hoje)
    sql_count = 'select count(trial_id) from submited_info where trial_id =  %s;' % trial_id

    cur.execute(sql_count)
    recset = cur.fetchall()
    ver = recset[0][0]
    print ver

    if ver == 0:
        print sql_insert
        cur.execute(sql_insert)
    else:
        print sql_update
        cur.execute(sql_update)

    con.commit()
    con.close()
    print ">>end  >>insere_data_submissao(%s,%s)" % (trial_id, db_file)

def search_sub_date(trial_id, db_file):
    print ">> search_sub_date(%s, %s)" % (trial_id, db_file)
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    sql_search = 'select trial_id, data_sub from submited_info where trial_id = %s;' %  trial_id
    cur.execute(sql_search)
    recset = cur.fetchall()
    print ">> end search_sub_date(%s, %s)" % (trial_id, db_file)
    if len(recset) == 0:
       return {"trial_id": trial_id, "sub_date": None}
    else:
       return {"trial_id": recset[0][0], "sub_date": recset[0][1]}



class PersisteDataSubmissao(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        dic_result_string =  "{'api_class': 'AvailableMail', 'result': %s}"
        trial_id = self.get_argument('trial_id', None)
        if trial_id:
                insere_data_submissao(trial_id, conf_dic["database_path"]+"/" +conf_dic["database"])

                self.write("{'persisted': True, 'trial_id': %s}" % trial_id)
        else:
                self.write("{msg: 'trial_id not pased'}")

class SearchDataSubmissao(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        dic_result_string =  "{'api_class': 'AvailableMail', 'result': %s}"
        trial_id = self.get_argument('trial_id', None)
        if trial_id:
                serch_dic = search_sub_date(trial_id, conf_dic["database_path"]+"/" +conf_dic["database"])

                self.write("%s" % serch_dic)
        else:
                self.write("{msg: 'trial_id not pased'}")

if __name__ == "__main__":
    pass
