import tornado.ioloop
import tornado.web
import os
import datetime
import sqlite3
from datetime import date
from settings import *


def insere_data_resubmissao(trial_id, db_file):
    print ">>insere_data_resubmissao(%s,%s)" % (trial_id, db_file)
    hoje = datetime.datetime.today()
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    sql_update = 'update resubmited_info set data_resub = "%s" where trial_id = %s;' % (hoje, trial_id)
    sql_insert = 'insert into resubmited_info values (%s, "%s");' % (trial_id, hoje)
    sql_count = 'select count(trial_id) from resubmited_info where trial_id =  %s;' % trial_id
    '''
    cur.execute(sql_count)
    recset = cur.fetchall()
    ver = recset[0][0]
    print ver

    if ver == 0:
        print sql_insert
        cur.execute(sql_insert)
    else:
        print sql_update
        cur.execute(sql_update)
    '''
    cur.execute(sql_insert)
    con.commit()
    con.close()
    print ">>end  >>insere_data_resubmissao(%s,%s)" % (trial_id, db_file)

def search_resub_date(trial_id, db_file):
    print ">> search_resub_date(%s, %s)" % (trial_id, db_file)
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    sql_search = 'select trial_id, data_resub from resubmited_info where trial_id = %s;' %  trial_id
    cur.execute(sql_search)
    recset = cur.fetchall()
    print ">> end search_resub_date(%s, %s)" % (trial_id, db_file)
    if len(recset) == 0:
       return {"trial_id": trial_id, "resub_date": None}
    else:
       return {"trial_id": recset[0][0], "resub_date": recset[0][1]}


class PersisteDataResubmissao(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        dic_result_string =  "{'api_class': 'AvailableMail', 'result': %s}"
        trial_id = self.get_argument('trial_id', None)
        if trial_id:
		insere_data_resubmissao(trial_id, conf_dic["database_path"]+"/" +conf_dic["database"])
	        
		self.write("{'persisted': True, 'trial_id': %s}" % trial_id)
        else:
                self.write("{msg: 'trial_id not pased'}")

class SearchDataResubmissao(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        dic_result_string =  "{'api_class': 'AvailableMail', 'result': %s}"
        trial_id = self.get_argument('trial_id', None)
        if trial_id:
		serch_dic = search_resub_date(trial_id, conf_dic["database_path"]+"/" +conf_dic["database"])

        	self.write("%s" % serch_dic)
        else:
                self.write("{msg: 'trial_id not pased'}")

def revisoes_por_revisor_dic(data_inicio, data_fim):
        #mensagem1 de log da tela1
        print ">> report_revisoes_por_revisor (%s,%s)" % (data_inicio, data_fim)
        #fim mensagem1 log tela

        #definindo queries
        query_resub = "select trial_id from resubmited_info where data_resub >= '%s' and data_resub <= '%s';" % (data_inicio, data_fim)
        query_user = "select revisor_name from revisor_info where trial_id = %s;"

        #executando query
        con = sqlite3.connect(conf_dic["database_path"]+"/" +conf_dic["database"])
        cur = con.cursor()
       
        #print mensagem2 log da tela
        print "query1: %s " % query_resub
        #fim mensagem2 log da tela
        
        cur.execute(query_resub)
        recset = cur.fetchall()



        dic_revisoes = {}
        for rec in recset:
            trial_id = rec[0]
            query_user_final = query_user % trial_id
            #print mensagem3 log da tela
            print "query2: %s " % query_user_final
            #fim mensagem3 log da tela
            cur.execute(query_user_final)
            result_user = cur.fetchall()
            if len(result_user) == 0:
                revisor_name = "undefined"
            else:
                revisor_name = result_user[0][0]

	    try:
                dic_revisoes[revisor_name].append(trial_id)
            except:
                dic_revisoes[revisor_name] = [trial_id]


	score_revisoes = {}
        total = 0
        for revisor in dic_revisoes:
            score_revisoes[revisor] = len(dic_revisoes[revisor])
            total = total + score_revisoes[revisor]
        
        score_revisoes["total"] = total

        #mensagem3 log tela
        print ">> fim report_revisoes_por_revisor"
        #fim log
        return score_revisoes

class ResubmissaoPorRevisor(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        data_inicial = self.get_argument('data_inicial', None)
        data_final = self.get_argument('data_final', None)
        
        score_dic = revisoes_por_revisor_dic(data_inicial, data_final)
        score_string = "%s" % score_dic
        self.write(score_string.replace("'", '"').replace('u"', '"'))




if __name__ == "__main__":
    pass
