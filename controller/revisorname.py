# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import os
import datetime
import sqlite3
from datetime import date
from settings import *


def insere_revisor_name(trial_id, revisor_name, db_file):
    print ">>insere_revisor_name(%s,%s, %s)" % (trial_id, revisor_name, db_file)
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    sql_update = 'update revisor_info set revisor_name = "%s" where trial_id = %s;' % (revisor_name, trial_id)
    sql_insert = 'insert into revisor_info values (%s, "%s");' % (trial_id, revisor_name)
    sql_count = 'select count(trial_id) from revisor_info where trial_id =  %s;' % trial_id

    cur.execute(sql_count)
    recset = cur.fetchall()
    ver = recset[0][0]
    print ver

    if ver == 0:
        print sql_insert
        cur.execute(sql_insert)
    else:
        print sql_update
        cur.execute(sql_update)

    con.commit()
    con.close()
    print ">>end  >>insere_revisor_name(%s,%s)" % (trial_id, db_file)

def search_revisor_name(trial_id, db_file):
    print ">> search_revisor_name(%s, %s)" % (trial_id, db_file)
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    sql_search = 'select revisor_name from revisor_info where trial_id = %s;' %  trial_id
    cur.execute(sql_search)
    recset = cur.fetchall()
    print ">> end search_sub_date(%s, %s)" % (trial_id, db_file)
   # '''
    if len(recset) == 0:
       return {"trial_id": trial_id, "revisor_name": conf_dic["revisors_list"]}
    else:
       return {"trial_id": trial_id, "revisor_name": recset[0][0]}
    #'''
    #para testar
    '''
    if trial_id%2 == 0:
        return "revisor_teste"
    else:
        return ["revisor1", "revisor2", "revisor3", "revisor4"]
    '''
class PersisteRevisorName(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        dic_result_string =  "{'api_class': 'AvailableMail', 'result': %s}"
        trial_id = self.get_argument('trial_id', None)
        revisor_name = self.get_argument('revisor_name', None)
        if trial_id and revisor_name:
                insere_revisor_name(trial_id,revisor_name, conf_dic["database_path"]+"/" +conf_dic["database"])

                self.write('{"persisted": 1, "trial_id": %s, "revisor_name": "%s"}' % (trial_id, revisor_name))
        else:
                self.write('{"persisted":0}')
'''
class SearchDataSubmissao(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        dic_result_string =  "{'api_class': 'AvailableMail', 'result': %s}"
        trial_id = self.get_argument('trial_id', None)
        if trial_id:
                serch_dic = search_sub_date(trial_id, "resubmited.db")

                self.write("%s" % serch_dic)
        else:
                self.write("{msg: 'trial_id not pased'}")
'''
if __name__ == "__main__":
    print search_revisor_name(123, conf_dic["database_path"]+"/" +conf_dic["database"])
