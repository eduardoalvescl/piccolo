# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import os
from report4 import *
import MySQLdb
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import datetime
import sqlite3
from datetime import date
from settings import conf_dic 


def sendMail(to, subject, text, files=[]):
    assert type(to)==list
    assert type(files)==list
    fro = "Expediteur <expediteur@mail.com>"

    msg = MIMEMultipart()
    msg['From'] = conf_dic["smtpuser"]
    msg['pass'] = conf_dic["smtppass"]
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )

    for file in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"'
                       % os.path.basename(file))
        msg.attach(part)

    SMTP_SERVER = conf_dic["smtpserver"]
    SMTP_PORT = conf_dic["smtpport"]
    smtp = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo
    smtp.login(msg['From'], msg["pass"])
    smtp.sendmail(fro, to, msg.as_string() )
    smtp.close()


class RescueContact(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))


        dic_result_string =  '{"api_class": "AvailableMail", "result": %s}'
        username = self.get_argument('username', 47)
        print ">> rescuecontact user: %s" % username
        if username == 47 or username == "":
            self.write(dic_result_string % '{"available": "username_not_passed"}')
        else:
	    query1 = 'delete from repository_publiccontact where contact_id ' \
	             'in (select id from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1);' % username

	    query_result1 = 'select count(id) from repository_publiccontact where contact_id ' \
                     'in (select id from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1);' % username

	    query2 = 'delete from repository_sitecontact where contact_id ' \
                     'in (select id from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1);' % username

	    query_result2 = 'select count(id) from repository_sitecontact where contact_id ' \
                     'in (select id from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1);' % username

	    query3 = 'delete from repository_scientificcontact where contact_id ' \
                     'in (select id from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1);' % username

            query_result3 = 'select count(id) from repository_scientificcontact where contact_id ' \
                     'in (select id from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1);' % username


	    query4 = 'delete from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1;' % username

	    
            query_result4 = 'select count(id) from repository_contact where creator_id = ' \
                     '(select id from auth_user where username like "%s") and _deleted = 1;' % username


            query_final1 = 'delete from repository_scientificcontact where contact_id in (select id from repository_contact where creator_id in (select id from auth_user where username = "%s")) and _deleted = 1;' % username

            print query_final1

            query_final2 = 'delete from repository_sitecontact where contact_id in (select id from repository_contact where creator_id in (select id from auth_user where username = "%s")) and _deleted = 1;' % username

            query_final3 = 'delete from repository_publiccontact where contact_id in (select id from repository_contact where creator_id in (select id from auth_user where username = "%s")) and _deleted = 1;' % username



            db = MySQLdb.connect(host= conf_dic["rebecdbhost"],
                      user= conf_dic["rebecdbuser"],
                      passwd= conf_dic["rebecdbpass"],
                      db= conf_dic["rebecdb"])
            cursor = db.cursor()

	    #deletting
            cursor.execute(query1)
            cursor.execute(query2)
            cursor.execute(query3)
            cursor.execute(query4)

	    #getting results
            cursor.execute(query_result1)
	    result1 = cursor.fetchall()[0][0]

	    cursor.execute(query_result2)
            result2 = cursor.fetchall()[0][0]

            cursor.execute(query_result3)
            result3 = cursor.fetchall()[0][0]

            cursor.execute(query_result3)
            result3 = cursor.fetchall()[0][0]

            cursor.execute(query_result4)
            result4 = cursor.fetchall()[0][0]

            cursor.execute(query_final1)
            cursor.execute(query_final2)
            cursor.execute(query_final3)

            str_dic = '{"public_result": %s, "site_result": %s, "sci_result": %s, "contact_result": %s}' % (result1, result2, result3, result4)
#            self.write("public: %s<br><br>site: %s<br><br>scientific: %s<br><br>contacts%s<br><br>" % (result1, result2, result3, result4))
	    self.write(str_dic)
            
	    #TODO informar usuario por email
	    query_is_user = "select count(id) from auth_user where username = '%s';" % username
	    cursor.execute(query_is_user)
            count_id = cursor.fetchall()[0][0]
            if int(count_id) > 0:
	        query_user = "select email from auth_user where username = '%s';" % username
                cursor.execute(query_user)
                user_mail = cursor.fetchall()[0][0]
                subject = "[REBEC] - Correção de Contatos"
                text = "Caro(a) usuário(a),\nHavia uma inconsistência no banco de dados gerada após um contato ser apagado\n" \
                       "da base antes de ser desvinculado de um contato público, de site ou científico.\n" \
                       "Fizemos uma correção diretamente na base e agora já é possível adcionar contatos.\n" \
		       "Att, \n\nEquipe Técnica Rebec" 
	        sendMail([user_mail, "rebec@icict.fiocruz.br", "dtostes@gmail.com"], subject, text, [])      
            db.close()
            print ">> end rescuecontact user: %s" % username
