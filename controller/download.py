import tornado.ioloop
import tornado.web
import os


class DownloadReport(tornado.web.RequestHandler):
    def get(self):
        file_name = gera_report()
        buf_size = 4096
        self.set_header('Content-Type', 'application/octet-stream')
        self.set_header('Content-Disposition', 'attachment; filename=' + file_name)
        with open(file_name, 'r') as f:
            while True:
                data = f.read(buf_size)
                if not data:
                    break
                self.write(data)
        self.finish()
	#os.system("rm %s" % file_name)

