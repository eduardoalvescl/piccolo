# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import os
from report4 import *
import MySQLdb
import datetime
import sqlite3
from datetime import date
from controller.submissao import PersisteDataSubmissao, SearchDataSubmissao, search_sub_date
from controller.resubmissao import PersisteDataResubmissao, SearchDataResubmissao
from controller.revisorname import PersisteRevisorName, search_revisor_name 
from controller.rescuecontact import RescueContact
from settings import conf_dic


def diffDate(HOJE, DATA_LIMITE):
    d1 = HOJE
    d2 = DATA_LIMITE
    delta = d2-d1
    r = delta.days
    return r

class RecuperarJson(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'application/json')
        self.set_header('Cache-Control', 'no-cache, must-revalidate')
        self.set_header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
        self.set_header("Access-Control-Allow-Headers", "Content-Type")
        self.add_header('Access-Control-Allow-Origin', self.request.headers.get('Origin', '*'))

        tipo_de_relatorio = self.get_argument('tipo_de_relatorio', 'asc')
        
        db = MySQLdb.connect(host= conf_dic["rebecdbhost"], #"localhost", # your host, usually localhost
                      user= conf_dic["rebecdbuser"],#"rebec", # your username
                      passwd= conf_dic["rebecdbpass"],#,"ensaios@12", # your password
                      db= conf_dic["rebecdb"])#,"opentrials_")
                      #port=3306, 
                      #unix_socket="/opt/lampp/var/mysql/mysql.sock") # name of the data base


        cur = db.cursor() 

       

        if (tipo_de_relatorio == 'relatorio_de_pendings'):
            query = ("select  sub.created, sub.trial_id,  sub.status,  rep.trial_id rbr, sub.updated updated_submission, (select username from auth_user where id = sub.creator_id) as username, (select email from auth_user where id = sub.creator_id) as email, concat('http://www.ensaiosclinicos.gov.br/rg/view/', sub.trial_id, '/')  as link_review, DATE_ADD(sub.created, INTERVAL 45 DAY) as dataLimite,sub.id as submissionId  from  reviewapp_submission sub, repository_clinicaltrial rep where sub.trial_id = rep.id and sub._deleted = 0 and sub.status = 'pending' order by rep.id asc;")
            cur.execute(query)
            lista = "["
            for row in cur.fetchall():
                resubmission_date = (search_sub_date(row[1],conf_dic["database_path"]+"/"+conf_dic["database"]))["sub_date"]
                if(resubmission_date):
                    resubmission_date = resubmission_date.split(" ")
                    resubmission_date = datetime.datetime.strptime(resubmission_date[0], "%Y-%m-%d")
                    TEMPO_PARA_REVISAO = 45 #quantidade de dias para que o registro seja revisado
                    limited_date = date.fromordinal(resubmission_date.toordinal()+TEMPO_PARA_REVISAO)
                    resubmission_date = resubmission_date.strftime('%Y-%m-%d')
                    #adcionar uma variavel com o valor de limited_date - dia_de_hoje
                    HOJE = datetime.date.today()
                    DIFF_DIAS_PARA_LIMITE = diffDate(HOJE, limited_date) 
                    resubmission = '{"resubmission_date":"%s","limited_date":"%s"}' % (resubmission_date,limited_date) 
                else:
                    resubmission = '0'
		    DIFF_DIAS_PARA_LIMITE = 'null'
		
		##
                DB = "%s/%s" % (conf_dic["database_path"],conf_dic["database"])
		revisor_name_dic = search_revisor_name(row[1],DB)
                if type(revisor_name_dic["revisor_name"]) != type([]):
                   revisor_name = '"%s"' % revisor_name_dic["revisor_name"]
                else:
                   revisor_name = str(revisor_name_dic["revisor_name"]).replace("'", '"')
                   
                lista+=('{"created":"%s","trial_id":"%s","status":"%s","rbr":"%s", "updated_submission":"%s", "username":"%s", "email":"%s", "link_review":"%s", "data_limite":"%s", "submissionId":"%i","resubmission":%s, "dias_para_revisao": %s, "revisor_name": %s},') % (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9],resubmission, DIFF_DIAS_PARA_LIMITE,revisor_name)

            lista = lista[:-1]
            lista+="]"
            self.write(lista)
        
        if(tipo_de_relatorio == 'relatorio_de_revisoes'):
            query = "select rev.created,  rev.context,  (select username from auth_user where id = rev.creator_id) as revisor, sub.trial_id, concat('http://www.ensaiosclinicos.gov.br/rg/view/', sub.trial_id, '/')  as link_review, sub.status, sub.updated, (select username from auth_user where id = sub.creator_id) as registrante_user, DATE_ADD(sub.updated, INTERVAL 45 DAY) as data_final from reviewapp_remark rev, reviewapp_submission sub where sub.id = rev.submission_id;"
            cur.execute(query)
            lista = "["
            for row in cur.fetchall():
                lista+=('{"created":"%s","context":"%s","revisor":"%s","trial_id":"%s", "link_review":"%s", "status":"%s", "updated":"%s", "registrante_user":"%s", "data_final":"%s"},') % (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8])

            lista = lista[:-1]
            lista+="]"

            self.write(lista)

    	if(tipo_de_relatorio == 'revisoes_por_revisor'):
    	    dataDeInicio = self.get_argument('dataDeInicio', '')
    	    dataDeFim = self.get_argument('dataDeFim', '')
            dataDeInicio = dataDeInicio.split('/')
            dataDeInicio = {"dia":dataDeInicio[1],"mes":dataDeInicio[0],"ano":dataDeInicio[2]}
            dataDeInicio = "%s/%s/%s" % (dataDeInicio['ano'],dataDeInicio['mes'],dataDeInicio['dia'])
            dataDeFim = dataDeFim.split('/')
            dataDeFim = {"dia":dataDeFim[1],"mes":dataDeFim[0],"ano":dataDeFim[2]}
            dataDeFim = "%s/%s/%s" %(dataDeFim['ano'],dataDeFim['mes'],dataDeFim['dia'])
	    
	    
    		#hj = datetime.datetime.today()
    		#passado = date.fromordinal(hj.toordinal()-7)
    		#dataDeInicio = '2013-08-18' #('%i-%i-%i') % (passado.year,passado.month,passado.day)
    		#dataDeFim = '2014-01-29' #('%i-%i-%i') % (hj.year,hj.month,hj.day)

    	    query = "SELECT creator_id AS idDoCriador, submission_id, (SELECT username FROM auth_user WHERE id = idDoCriador) FROM reviewapp_remark WHERE created <= '%s' AND created >= '%s';" % (dataDeFim,dataDeInicio)
    	    cur.execute(query)
    	    dic  = {}
            total = 0
    	    for row in cur.fetchall():
    		try:
    	  	    dic[row[2]] = dic[row[2]] + 1
         	except:
    		    dic[row[2]] = 1

                total = total + 1

            dic["total"] = total

    	    lista = "{"

    	    for nome,valor in dic.items():
    		     lista+=(('"%s":%i') % (nome,valor))
    		     lista+=','


            lista = lista[:-1]

    	    lista+="}"
    		
    	    self.write(lista)

        if(tipo_de_relatorio == 'campos_mais_revisados'):
                #query -> select context from reviewapp_remark where created <='2014/01/29' and created >= '2013/08/18'
                dataDeInicio = self.get_argument('dataDeInicio', '')
                dataDeFim = self.get_argument('dataDeFim', '')
                dataDeInicio = dataDeInicio.split('/')
                dataDeInicio = {"dia":dataDeInicio[1],"mes":dataDeInicio[0],"ano":dataDeInicio[2]}
                dataDeInicio = "%s/%s/%s" % (dataDeInicio['ano'],dataDeInicio['mes'],dataDeInicio['dia'])
                dataDeFim = dataDeFim.split('/')
                dataDeFim = {"dia":dataDeFim[1],"mes":dataDeFim[0],"ano":dataDeFim[2]}
                dataDeFim = "%s/%s/%s" %(dataDeFim['ano'],dataDeFim['mes'],dataDeFim['dia'])
            
            
                #hj = datetime.datetime.today()
                #passado = date.fromordinal(hj.toordinal()-7)
                #dataDeInicio = '2013-08-18' #('%i-%i-%i') % (passado.year,passado.month,passado.day)
                #dataDeFim = '2014-01-29' #('%i-%i-%i') % (hj.year,hj.month,hj.day)

                query = "select context from reviewapp_remark where created <= '%s' AND created >= '%s'" % (dataDeFim,dataDeInicio)
                cur.execute(query)
                dic  = {}
                total = 0
                for row in cur.fetchall():
                    try:
                        dic[row[0]] = dic[row[0]] + 1
                    except:
                        dic[row[0]] = 1

                    total = total + 1

                dic["total"] = total

                lista = "{"

                for nome,valor in dic.items():
                     lista+=(('"%s":%i') % (nome,valor))
                     lista+=','


                lista = lista[:-1]

                lista+="}"

                print "--------------------%s" % lista
                
                self.write(lista)
