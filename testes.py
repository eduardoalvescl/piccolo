# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import os
from controller.submissao import PersisteDataSubmissao, SearchDataSubmissao, search_sub_date
from controller.resubmissao import PersisteDataResubmissao, SearchDataResubmissao, revisoes_por_revisor_dic
from controller.revisorname import PersisteRevisorName, search_revisor_name
from controller.rescuecontact import RescueContact
from controller.grid_info import RecuperarJson
from controller.download import DownloadReport
from controller.renderhtml import retorna_pendings
from controller.created_trials_report import ReportCreatedTrials
from settings import conf_dic



if __name__ == "__main__":
    print revisoes_por_revisor_dic("2014-02-02", "2014-05-02")
