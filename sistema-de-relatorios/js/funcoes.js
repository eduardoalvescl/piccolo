	listaDeRevisores = "['adm_daniel','adm_gian','adm_josue','adm_marcelo','adm_vanessa']"; // listagem de administradores
function definirRevisor(trial_id){ // função responsável por definir o revisor responsável por um ensaio. Recebe um trial_id referente ao ensaio que está em pending
	var nomeDoRevisor = $("#revisorResponsavel_"+trial_id+" option:selected").val(); 
	$('#loadingRevisores_'+trial_id).show();
	$.getJSON("gravarevisorname?trial_id="+trial_id+"&revisor_name="+nomeDoRevisor, function(json) {
			
		if(json.persisted == 1){
			$('#revisorResponsavel_'+trial_id).html('<p>'+json.revisor_name+'</p><br><input type="button" class="editarRevisorResponsavel" id="editar_'+trial_id+'" onclick="editarRevisor(\''+trial_id+'\','+listaDeRevisores+')" value="editar"><div id="loadingRevisores_'+trial_id+'" style="display:none;"><img src="static/preloader-revisores.gif"></div>');
		}
		else if(json.persisted == 0){
			alert('Ocorreu um erro!');
		}
		$('#loadingRevisores_'+trial_id).hide();
	
	});
	}

function editarRevisor(trial_id,revisores){ // função responsável por editar o revisor responsável pelo ensaio. Recebe um trial_id referente ao ensaio em pending e a lista de revisores responsáveis.
	var nomeDoRevisor = '<select id="trial_id_'+trial_id+'">';
	for(var x=0;x<revisores.length;x++){
		nomeDoRevisor = nomeDoRevisor +'<option value="'+revisores[x]+'">'+ revisores[x]+'</option>';
	}
	var nomeDoRevisorNaDiv = $('#revisorResponsavel_'+trial_id+' p').html();
	
	nomeDoRevisor = nomeDoRevisor + '</select><br><input type="button" class="salvarRevisorResponsavel" id="'+trial_id+'" onclick="definirRevisor(\''+trial_id+'\')" value="OK">&nbsp;<input type="button" class="cancelarEdicaoRevisorResponsavel" id="'+trial_id+'" onclick="cancelarEdicaoRevisao(\''+trial_id+'\',\''+nomeDoRevisorNaDiv+'\')" value="cancelar"><div id="loadingRevisores_'+trial_id+'" style="display:none;"><img src="static/preloader-revisores.gif></div>';
	$('#revisorResponsavel_'+trial_id).html(nomeDoRevisor);
	$('#trial_id_'+trial_id+' option[value="'+nomeDoRevisorNaDiv+'"]').prop('selected', true);
}


function cancelarEdicaoRevisao(trial_id,nomeDoRevisor){ // Função que cancela a edição e volta ao estado anterior
	$('#revisorResponsavel_'+trial_id).html('<p>'+nomeDoRevisor+'</p><br><input type="button" class="editarRevisorResponsavel" id="editar_'+trial_id+'" onclick="editarRevisor(\''+trial_id+'\','+listaDeRevisores+')" value="editar"><div id="loadingRevisores_'+trial_id+'" style="display:none;"><img src="static/preloader-revisores.gif"></div>');

}


$(document).ready(function(){
$('.filtrarRevisor').click(function(){ // função responsável por filtrar os ensaios por usuário
	relatorio_de_pendings_com_filtro($("#selectDoRevisor option:selected").val()); //chama a função relatorio_de_pendings_com_filtro e passa como parâmetro o nome do revisor selecionado no campo select
});

function relatorio_de_pendings_com_filtro(username){ // exibe a tabela filtrada por revisor. Tem como parâmetro o nome do revisor
$('.quantidadeDeVerdes').html(0);
$('.quantidadeDeAmarelos').html(0);
$('.quantidadeDeVermelhos').html(0);
$('.graficoDeCores').hide();
$('#menu a').removeClass('abaAtiva');
$('.carregarTabelaDePendings').addClass('abaAtiva');
$('.final div').remove();
$('.final table').remove();
$('.grafico').remove();
$('#chart1').html('');
$('.final').append('<div class="tabelaDePendingsDiv"><table class="tabelaDePendings" id="tabelaDePendings" cellspacing="0"><thead><tr><td class="cabecalhoTabela">created</td><td class="cabecalhoTabela">trial_id</td><td class="cabecalhoTabela">rbr</td><td class="cabecalhoTabela">updated_submission</td><td class="cabecalhoTabela">username</td><td class="cabecalhoTabela">email</td><td class="cabecalhoTabela">link_review</td><td class="cabecalhoTabela">submissionId</td><td class="cabecalhoTabela">submission_date</td><td class="cabecalhoTabela">limited_date</td><td class="cabecalhoTabela">Dias restantes</td><td class="cabecalhoTabela">Nome do Revisor</td></tr></thead><tbody></tbody></table></div>');
$('.loader').css('display','inline');
$('.final').hide();

$('.formulario').hide();
$('h1').html('Tabela de Pendings ('+username+')');

$.getJSON("retornaj?tipo_de_relatorio=relatorio_de_pendings", function(json) {
var nomeDoRevisor;
for(var i=0;i<json.length;i++){
var color;

// Seleciona que ensaios já tem revisor e os que não tem
	if(json[i].revisor_name == username){

		if(json[i].dias_para_revisao <= 45 && json[i].dias_para_revisao >= 20){
color = ['#00FF00','#000','green'];
resultado = 'expira em '+json[i].dias_para_revisao+' dias';
$('.quantidadeDeVerdes').html(parseInt($('.quantidadeDeVerdes').html())+1);
}
else if(json[i].dias_para_revisao <= 20 && json[i].dias_para_revisao > 0){
color = ['#FF0','#000','yellow'];
resultado = 'expira em '+json[i].dias_para_revisao+' dias';
$('.quantidadeDeAmarelos').html(parseInt($('.quantidadeDeAmarelos').text())+1);
}
else if(json[i].dias_para_revisao == 0){
color = ['#FF0000','#000','red'];
resultado = 'expira Hoje';
$('.quantidadeDeVermelhos').html(parseInt($('.quantidadeDeVermelhos').text())+1);
}
else if(json[i].dias_para_revisao <= 0){
color = ['#FF0000','#000','red'];
resultado = 'expirou a '+(json[i].dias_para_revisao*-1)+' dias';
$('.quantidadeDeVermelhos').html(parseInt($('.quantidadeDeVermelhos').text())+1);
}
else{color = '#FFF';}
		if(typeof(json[i].revisor_name) == 'object'){

			nomeDoRevisor = '<div id="revisorResponsavel_'+json[i].trial_id+'"><select id="trial_id_'+json[i].trial_id+'">';
			for(var x=0;x<json[i].revisor_name.length;x++){
				nomeDoRevisor = nomeDoRevisor +'<option value="'+json[i].revisor_name[x]+'">'+ json[i].revisor_name[x]+'</option>';
			}
			nomeDoRevisor = nomeDoRevisor + '</select>&nbsp;<input type="button" class="salvarRevisorResponsavel" id="'+json[i].trial_id+'" onclick="definirRevisor(\''+json[i].trial_id+'\')" value="OK"><div id="loadingRevisores_'+json[i].trial_id+'" style="display:none;"><img src="static/preloader-revisores.gif"></div></div>';
		}else{
			nomeDoRevisor = '<div id="revisorResponsavel_'+json[i].trial_id+'"><p>'+json[i].revisor_name+'</p><br><input type="button" class="editarRevisorResponsavel" id="editar_'+json[i].trial_id+'" onclick="editarRevisor(\''+json[i].trial_id+'\','+listaDeRevisores+')" value="editar"><div id="loadingRevisores_'+json[i].trial_id+'" style="display:none;"><img src="static/preloader-revisores.gif"></div></div>';
		}
		// fim da codificação que Seleciona que ensaios já tem revisor e os que não tem
		
		$('.tabelaDePendings tbody').append('<tr><td>'+json[i].created+'</td><td>'+json[i].trial_id+'</td><td>'+json[i].rbr+'</td><td>'+json[i].updated_submission+'</td><td>'+json[i].username+'</td><td>'+json[i].email+'</td><td><a href="'+json[i].link_review+'" target="_blank">Link para a revisão</a></td><td>'+json[i].submissionId+'</td><td>'+json[i].resubmission["resubmission_date"]+'</td><td>'+json[i].resubmission["limited_date"]+'</td><td style="background-color:'+color[0]+';color:'+color[1]+'" class="'+color[2]+'">'+resultado+'</td><td>'+nomeDoRevisor+'</td></tr>');
	}
}
$('.loader').hide();
$('#tabelaDePendings').dataTable({
		"sType": "html",
        "oSearch": {"bSmart": false}
    });
$('.final').show();
});

}

function resgatar_relatorio(tipo_de_relatorio){//função gera um relatório na tela. Tem como parâmetro o tipo de relatório
if(tipo_de_relatorio == "relatorio_de_pendings"){ //exibe o relatório de pendings
		
		$('.formulario').hide();
		$('#chart1').html('');
		$('h1').html('Tabela de Pendings');

		$('.quantidadeDeVerdes').html(0);
		$('.quantidadeDeAmarelos').html(0);
		$('.quantidadeDeVermelhos').html(0);

		$.getJSON("retornaj?tipo_de_relatorio=relatorio_de_pendings", function(json) {
			$('#filtro').show();
			var nomeDoRevisor;
			for(var i=0;i<json.length;i++){
			var color;
			if(json[i].dias_para_revisao <= 45 && json[i].dias_para_revisao >= 20){
			color = ['#00FF00','#000','green'];
			resultado = 'expira em '+json[i].dias_para_revisao+' dias';
			$('.quantidadeDeVerdes').html(parseInt($('.quantidadeDeVerdes').html())+1);
			}
			else if(json[i].dias_para_revisao <= 20 && json[i].dias_para_revisao > 0){
			color = ['#FF0','#000','yellow'];
			resultado = 'expira em '+json[i].dias_para_revisao+' dias';
			$('.quantidadeDeAmarelos').html(parseInt($('.quantidadeDeAmarelos').text())+1);
			}
			else if(json[i].dias_para_revisao == 0){
			color = ['#FF0000','#000','red'];
			resultado = 'expira Hoje';
			$('.quantidadeDeVermelhos').html(parseInt($('.quantidadeDeVermelhos').text())+1);
			}
			else if(json[i].dias_para_revisao <= 0){
			color = ['#FF0000','#000','red'];
			resultado = 'expirou a '+(json[i].dias_para_revisao*-1)+' dias';
			$('.quantidadeDeVermelhos').html(parseInt($('.quantidadeDeVermelhos').text())+1);
			}
			else{color = '#FFF';}
			// Seleciona que ensaios já tem revisor e os que não tem
			if(typeof(json[i].revisor_name) == 'object'){

				nomeDoRevisor = '<div id="revisorResponsavel_'+json[i].trial_id+'"><select id="trial_id_'+json[i].trial_id+'">';
				for(var x=0;x<json[i].revisor_name.length;x++){
					nomeDoRevisor = nomeDoRevisor +'<option value="'+json[i].revisor_name[x]+'">'+ json[i].revisor_name[x]+'</option>';
				}
				nomeDoRevisor = nomeDoRevisor + '</select>&nbsp;<input type="button" class="salvarRevisorResponsavel" id="'+json[i].trial_id+'" onclick="definirRevisor(\''+json[i].trial_id+'\')" value="OK"><div id="loadingRevisores_'+json[i].trial_id+'" style="display:none;"><img src="static/preloader-revisores.gif"></div></div>';
			}else{
				nomeDoRevisor = '<div id="revisorResponsavel_'+json[i].trial_id+'"><p>'+json[i].revisor_name+'</p><br><input type="button" class="editarRevisorResponsavel" id="editar_'+json[i].trial_id+'" onclick="editarRevisor(\''+json[i].trial_id+'\','+listaDeRevisores+')" value="editar"><div id="loadingRevisores_'+json[i].trial_id+'" style="display:none;"><img src="static/preloader-revisores.gif"></div></div>';
			}
			// fim da codificação que Seleciona que ensaios já tem revisor e os que não tem

			$('.tabelaDePendings tbody').append('<tr><td>'+json[i].created+'</td><td>'+json[i].trial_id+'</td><td>'+json[i].rbr+'</td><td>'+json[i].updated_submission+'</td><td>'+json[i].username+'</td><td>'+json[i].email+'</td><td><a href="'+json[i].link_review+'" target="_blank">Link para a revisão</a></td><td>'+json[i].submissionId+'</td><td>'+json[i].resubmission["resubmission_date"]+'</td><td>'+json[i].resubmission["limited_date"]+'</td><td style="background-color:'+color[0]+';color:'+color[1]+'" class="'+color[2]+'">'+resultado+'</td><td>'+nomeDoRevisor+'</td></tr>');
			}
			$('.loader').hide();
			$('#tabelaDePendings').dataTable({
					"sType": "html",
			        "oSearch": {"bSmart": false}
			    });
			$('.final').show();
		});
}

if(tipo_de_relatorio == "relatorio_de_revisoes"){ //exibe o relatório de revisões
	$('#chart1').html('');
$('.formulario').hide();
$('h1').html('Tabela de Revisões');
$.getJSON("retornaj?tipo_de_relatorio=relatorio_de_revisoes", function(json) {
for(var i=0;i<json.length;i++){
$('.tabelaDePendings tbody').append('<tr><td>'+json[i].created+'</td><td>'+json[i].context+'</td><td>'+json[i].revisor+'</td><td>'+json[i].trial_id+'</td><td>'+json[i].link_review+'</td><td>'+json[i].updated+'</td><td>'+json[i].registrante_user+'</a></td><td>'+json[i].data_final+'</td><td>Nome do Revisor</td></tr>');
}
$('.loader').hide();
$('#tabelaDePendings').dataTable( {
        "oSearch": {"sType": "html","bSmart": false}
    });
$('.final').show();
});
}
if(tipo_de_relatorio == "zerar_tabelas_de_contato"){ //função que resolve o problema da tabela de contatos do sistema. Desvincula contatos.
	$('#chart1').html('');
$('.formulario').hide();
$('h1').html('Zerar Tabelas de Contato');
$('.loader').hide();
$('.final').show();
$('#menu a').removeClass('abaAtiva');
$('.carregarZerarTabelasDeContato').addClass('abaAtiva');
}
if(tipo_de_relatorio == "revisoes_por_revisor"){ // exibe os campos para a pesquisa do relatório de revisões por revisor
	$('#chart1').html('');
$('.formulario').show();
$('h1').html('Relatorio de rendimento');
$('#menu a').removeClass('abaAtiva');
$('.carregarGraficos').addClass('abaAtiva');
$('.loader').hide();
$('.final').show();
$( "#dataDeInicio" ).datepicker();
$( "#dataDeFim" ).datepicker();
}
}
$('.carregarTabelaDePendings').click(function(){ // Carrega tabela de pendings
	$('#chart1').html('');
$('#menu a').removeClass('abaAtiva');
$('.carregarTabelaDePendings').addClass('abaAtiva');
$('.final div').remove();
$('.final table').remove();
$('.grafico').remove();
$('#chart1').html('');
$('.final').append('<div class="tabelaDePendingsDiv"><table class="tabelaDePendings" id="tabelaDePendings" cellspacing="0"><thead><tr><td class="cabecalhoTabela">created</td><td class="cabecalhoTabela">trial_id</td><td class="cabecalhoTabela">rbr</td><td class="cabecalhoTabela">updated_submission</td><td class="cabecalhoTabela">username</td><td class="cabecalhoTabela">email</td><td class="cabecalhoTabela">link_review</td><td class="cabecalhoTabela">submissionId</td><td class="cabecalhoTabela">submission_date</td><td class="cabecalhoTabela">limited_date</td><td class="cabecalhoTabela">Dias restantes</td><td class="cabecalhoTabela">Nome do Revisor</td></tr></thead><tbody></tbody></table></div>');
$('.loader').css('display','inline');
$('.final').hide();
resgatar_relatorio("relatorio_de_pendings");
$('.graficoDeCores').hide();
});






$('.carregarTabelaDeRevisoes').click(function(){ // Carrega a tabela de revisões
	$('#chart1').html('');
$('#menu a').removeClass('abaAtiva');
$('.carregarTabelaDeRevisoes').addClass('abaAtiva');
$('.final div').remove();
$('.final table').remove();
$('.grafico').remove();
$('#chart1').html('');
$('.final').append('<table class="tabelaDePendings" id="tabelaDePendings" cellspacing="0"><thead><tr><td class="cabecalhoTabela">created</td><td class="cabecalhoTabela">context</td><td class="cabecalhoTabela">revisor</td><td class="cabecalhoTabela">trial_id</td><td class="cabecalhoTabela">link_review</td><td class="cabecalhoTabela">updated</td><td class="cabecalhoTabela">registrante_user</td><td class="cabecalhoTabela">data_final</td></tr></thead><tbody></tbody></table>');
$('.loader').show();
$('.final').hide();
resgatar_relatorio("relatorio_de_revisoes");
$('.graficoDeCores').hide();
});
$('.final').on('click','.enviarZerarTabelaDeContatos',function(){ // Envia o username para o servidor e retorna se conseguir desvincular a tabela de contatos ou não(alert)
var username = $('#username').val();
$.getJSON("rescue?username="+username, function(json) {
if(json.public_result == 0 && json.site_result == 0 && json.sci_result == 0 && json.contact_result == 0){
alert('Tabelas Zeradas Com Sucesso');
}
else{
alert("Ocorreu um erro! Verifique se o username está correto");
}
});
});
$('.carregarGraficos').click(function(){ // Carrega os dados do gráfico revisões por revisor
$('#filtro').hide();
$('.final div').remove();
$('.final table').remove();
$('.loader').show();
$('.final').hide();
resgatar_relatorio("revisoes_por_revisor");
$('.graficoDeCores').hide();
});
$('.carregarZerarTabelasDeContato').click(function(){ // Carrega o formulário para a desvinculação de contatos
$('#filtro').hide();
$('#menu a').removeClass('abaAtiva');
$('.carregarTabelaDeRevisoes').addClass('abaAtiva');
$('.final div').remove();
$('.final table').remove();
$('.grafico').remove();
$('#chart1').html('');
$('.final').append('<table><tr><td>Digite o username do registrante:</td></tr><tr><td><input type="text" id="username"></td></tr><tr><td><a class="enviarZerarTabelaDeContatos" href="#">Enviar</a></td></tr></table>');
$('.loader').show();
$('.final').hide();
resgatar_relatorio("zerar_tabelas_de_contato");
$('.graficoDeCores').hide();
});
$('.gerarGrafico').click(function(){ //função responsável por gerar os gráficos ao clicar em qualquer objeto que tenha a classe .gerarGrafico
var dataDeInicio = $('#dataDeInicio').val();
var dataDeFim = $('#dataDeFim').val();
var tituloDoRelatorio = $ ( ".selectDoRelatorio option:selected").val();
var tituloDoRelatorioLabel = $ ( ".selectDoRelatorio option:selected").html();
$('.grafico').remove();
$('#chart1').html('');
$('#chart1').html('');
	if(tituloDoRelatorio == "revisoes_por_revisor" || tituloDoRelatorio == "campos_mais_revisados"){

			$.getJSON("retornaj?tipo_de_relatorio="+tituloDoRelatorio+"&dataDeInicio="+dataDeInicio+"&dataDeFim="+dataDeFim, function(json) {
			var usuarios;
			var valores;
			for (var k in json){
			if (typeof(json[k]) !== 'function' && typeof(json[k]) !== 'undefined' && k !== "total") {
			usuarios = usuarios+'|'+k;
			valores = valores+','+(json[k])/100;
			}
			}
			usuarios = usuarios.replace("undefined|","");
			valores = valores.replace("undefined,","");
			$('.final').append('<div class="grafico"><div class="numeros-graficos"><h2 style="padding:20px 0 20px 20px;background:#000;color:#FFF;float:left;width:95%;font-size:20px;"></h2><span style="dispĺay:inline;margin:10px 0 0 0;padding:20px 20px 20px 20px;background:#000;color:#FFF;float:left;font-size:15px;"></span><br><div style="clear:both;"></div><br><ul style="float:left;margin:10px 0 0 20px;font-size:18px;"></ul></div><img style="float:right;" src="http://chart.apis.google.com/chart?cht=p&chxt=x,y&chd=t:'+valores+'&chco=555&chs=600x425&chxl=0:|'+usuarios+'"></div>');
				for (var k in json){
					if (typeof(json[k]) !== 'function' && typeof(json[k]) !== 'undefined' && k !== "total") {
						$('.numeros-graficos ul').append('<li>'+k+':'+json[k]+'</li>');
					}
					else if(k == "total"){
						$('.numeros-graficos span').append('<b>'+k+':'+json[k]+'</b>');
					}
				}
				$('#menu a').removeClass('abaAtiva');
				$('.carregarGraficos').addClass('abaAtiva');
				$('.loader').hide();
				$( "#dataDeInicio" ).datepicker();
				$( "#dataDeFim" ).datepicker();
				$('.final').show();
				$('h2').html(tituloDoRelatorioLabel);
				$( "#dataDeInicio" ).datepicker();
				$( "#dataDeFim" ).datepicker();
				});



	}else if(tituloDoRelatorio == "ressubmissoes-por-revisor"){
			dataDeInicio = dataDeInicio.split('/');
			dataDeInicio = dataDeInicio[2]+'-'+dataDeInicio[0]+'-'+dataDeInicio[1];
			dataDeFim = dataDeFim.split('/');
			dataDeFim = dataDeFim[2]+'-'+dataDeFim[0]+'-'+dataDeFim[1];
			var endereco_host = 'resubrevisor?data_inicial='+dataDeInicio+'&data_final='+dataDeFim;
			$.getJSON(endereco_host, function(json) {

				if(json['total']==0){
					alert('Nenhuma resubmissão registrada no momento!');
				}else{
				var usuarios;
				var valores;
				for (var k in json){
				if (typeof(json[k]) !== 'function' && typeof(json[k]) !== 'undefined' && k !== "total") {
				usuarios = usuarios+'|'+k;
				valores = valores+','+json[k];
				}
				}

			
				usuarios = usuarios.replace("undefined|","");
				valores = valores.replace("undefined,","");
				
				    $('.final').append('<div class="grafico"><div class="numeros-graficos"><h2 style="padding:20px 0 20px 20px;background:#000;color:#FFF;float:left;width:95%;font-size:20px;"></h2><span style="dispĺay:inline;margin:10px 0 0 0;padding:20px 20px 20px 20px;background:#000;color:#FFF;float:left;font-size:15px;"></span><br><div style="clear:both;"></div><br><ul style="float:left;margin:10px 0 0 20px;font-size:18px;"></ul></div><img style="float:right;" src="http://chart.apis.google.com/chart?cht=p&chxt=x,y&chd=t:'+valores+'&chco=555&chs=600x425&chxl=0:|'+usuarios+'"></div>');
				
					for (var k in json){
						if (typeof(json[k]) !== 'function' && typeof(json[k]) !== 'undefined' && k !== "total") {
							$('.numeros-graficos ul').append('<li>'+k+':'+json[k]+'</li>');
						}
						else if(k == "total"){
							$('.numeros-graficos span').append('<b>'+k+':'+json[k]+'</b>');						
						}
					}

					$('#menu a').removeClass('abaAtiva');
					$('.carregarGraficos').addClass('abaAtiva');
					$('.loader').hide();
					$( "#dataDeInicio" ).datepicker();
					$( "#dataDeFim" ).datepicker();
					$('.final').show();
					$('h2').html('Resubmissões por Revisor');
					$( "#dataDeInicio" ).datepicker();
					$( "#dataDeFim" ).datepicker();
				}
			});

				

	}

	else if(tituloDoRelatorio == "aprovacoes-por-revisor"){
			dataDeInicio = dataDeInicio.split('/');
			dataDeInicio = dataDeInicio[2]+'-'+dataDeInicio[0]+'-'+dataDeInicio[1];
			dataDeFim = dataDeFim.split('/');
			dataDeFim = dataDeFim[2]+'-'+dataDeFim[0]+'-'+dataDeFim[1];
			var endereco_host = 'aprovrevisor?data_inicial='+dataDeInicio+'&data_final='+dataDeFim;
			$.getJSON(endereco_host, function(json) {

				if(json['total']==0){
					alert('Nenhuma aprovação registrada no momento!');
				}else{
				var usuarios;
				var valores;
				for (var k in json){
				if (typeof(json[k]) !== 'function' && typeof(json[k]) !== 'undefined' && k !== "total") {
				usuarios = usuarios+'|'+k;
				valores = valores+','+json[k];
				}
				}

			
				usuarios = usuarios.replace("undefined|","");
				valores = valores.replace("undefined,","");
				
				    $('.final').append('<div class="grafico"><div class="numeros-graficos"><h2 style="padding:20px 0 20px 20px;background:#000;color:#FFF;float:left;width:95%;font-size:20px;"></h2><span style="dispĺay:inline;margin:10px 0 0 0;padding:20px 20px 20px 20px;background:#000;color:#FFF;float:left;font-size:15px;"></span><br><div style="clear:both;"></div><br><ul style="float:left;margin:10px 0 0 20px;font-size:18px;"></ul></div><img style="float:right;" src="http://chart.apis.google.com/chart?cht=p&chxt=x,y&chd=t:'+valores+'&chco=555&chs=600x425&chxl=0:|'+usuarios+'"></div>');
				
					for (var k in json){
						if (typeof(json[k]) !== 'function' && typeof(json[k]) !== 'undefined' && k !== "total") {
							$('.numeros-graficos ul').append('<li>'+k+':'+json[k]+'</li>');
						}
						else if(k == "total"){
							$('.numeros-graficos span').append('<b>'+k+':'+json[k]+'</b>');						
						}
					}

					$('#menu a').removeClass('abaAtiva');
					$('.carregarGraficos').addClass('abaAtiva');
					$('.loader').hide();
					$( "#dataDeInicio" ).datepicker();
					$( "#dataDeFim" ).datepicker();
					$('.final').show();
					$('h2').html('Aprovações por Revisor');
					$( "#dataDeInicio" ).datepicker();
					$( "#dataDeFim" ).datepicker();
				}
			});

				

	}
});
	$('.porcentagemDeCores').click(function(){
		var vermelho = parseInt($('.quantidadeDeVermelhos').html());
	    var amarelo = parseInt($('.quantidadeDeAmarelos').html());
		var verde = parseInt($('.quantidadeDeVerdes').html());
		var total = vermelho+amarelo+verde;

		vermelho = Math.round((vermelho/total)*100);
		amarelo = Math.round((amarelo/total)*100);
		verde = Math.round((verde/total)*100);

		$('.vermelho').css('width',vermelho+'%').html(vermelho+'%');
		$('.amarelo').css('width',amarelo+'%').html(amarelo+'%');
		$('.verde').css('width',verde+'%').html(verde+'%');

		$('.graficoDeCores').toggle();

	});

	$('#selecionarRelatorio').change(function(){

		if($('#selecionarRelatorio option:checked').val() == 'ensaios-por-mes'){

			$('.dataDeInicio,.dataDeFim').hide();
			$('.anoDoRelatorio').show();
			$('#valor-do-ano').show();
			$('.gerarGrafico').hide();
			$('.criar-grafico-por-ano').show();
			$('#chart1').html('');
			$('.grafico').remove();

		} else {
			$('.dataDeInicio,.dataDeFim').show();
			$('.anoDoRelatorio').hide();
			$('#valor-do-ano').hide();
			$('.gerarGrafico').show();
			$('.criar-grafico-por-ano').hide();
			$('#chart1').html('');
		}

	});


			$('.criar-grafico-por-ano').click(function(){
				$('#chart1').html('');

				var ano = $("#valor-do-ano option:selected").val();

				$.getJSON("reportensaios?ano="+ano, function(json) {
					var jan = (json.jan == 'undefined')?0:json.jan;
					var fev = (json.fev == 'undefined')?0:json.fev;
					var mar = (json.mar == 'undefined')?0:json.mar;
					var abr = (json.abr == 'undefined')?0:json.abr;
					var mai = (json.mai == 'undefined')?0:json.mai;
					var jun = (json.jun == 'undefined')?0:json.jun;
					var jul = (json.jul == 'undefined')?0:json.jul;
					var ago = (json.ago == 'undefined')?0:json.ago;
					var set = (json.set == 'undefined')?0:json.set;
					var out = (json.out == 'undefined')?0:json.out;
					var nov = (json.nov == 'undefined')?0:json.nov;
					var dez = (json.dez == 'undefined')?0:json.dez;

				

							    var line3 = [['jan('+jan+')', jan], ['fev('+fev+')', fev],['mar('+mar+')', mar], ['abr('+abr+')', abr], 
							    ['mai('+mai+')', mai], ['jun('+jun+')', jun], ['jul('+jul+')', jul], ['ago('+ago+')', ago],['set('+set+')', set],['out('+out+')', out],['nov('+nov+')', nov],['dez('+dez+')', dez]];
							 
							    var plot3 = $.jqplot('chart1', [line3], {
							      series:[{lineWidth:2, markerOptions:{style:'square'}}],
							      axes: {
							        xaxis: {
							          renderer: $.jqplot.CategoryAxisRenderer,
							          label: '2014',
							          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
							          tickRenderer: $.jqplot.CanvasAxisTickRenderer,
							          tickOptions: {
							              angle: -40,
							              fontFamily: 'Courier New',
							              fontSize: '20px'
							          }
							           
							        },
							        yaxis: {
							          label: '',
							          labelRenderer: $.jqplot.CanvasAxisLabelRenderer
							        }
							      }
							    });
				});
		 	});
		
});
  	 