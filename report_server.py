# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
import os
from controller.submissao import PersisteDataSubmissao, SearchDataSubmissao, search_sub_date
from controller.resubmissao import PersisteDataResubmissao, SearchDataResubmissao, ResubmissaoPorRevisor
from controller.aprovacao import PersisteDataAprovacao, AprovacaoPorRevisor
from controller.revisorname import PersisteRevisorName, search_revisor_name 
from controller.rescuecontact import RescueContact
from controller.grid_info import RecuperarJson
from controller.download import DownloadReport
from controller.renderhtml import retorna_pendings
from controller.created_trials_report import ReportCreatedTrials
from settings import conf_dic
from verify_settings import check_js_settings

application = tornado.web.Application([
    (r"/", retorna_pendings),
    (r"/reportrevisao", DownloadReport),
    (r"/rescue", RescueContact),
    (r"/retornaj", RecuperarJson),
    (r"/retorna_pendings", retorna_pendings),
    (r"/gravadatar", PersisteDataResubmissao),
    (r"/recuperadatar", SearchDataResubmissao),
    (r"/gravadatas", PersisteDataSubmissao),
    (r"/recuperadatas", SearchDataSubmissao),
    (r"/gravarevisorname", PersisteRevisorName),
    (r"/reportensaios", ReportCreatedTrials),
    (r"/resubrevisor", ResubmissaoPorRevisor),
    (r"/gravadataaprova", PersisteDataAprovacao),
    (r"/aprovrevisor", AprovacaoPorRevisor),
    ],
    template_path=os.path.join(os.path.dirname(__file__), ""),
    static_path=os.path.join(os.path.dirname(__file__), "eduego"),
    debug=True
)

if __name__ == "__main__":
    check_js_settings()
    port = conf_dic["appport"] 
    application.listen(port)
    print "server running over %s port" % port
    tornado.ioloop.IOLoop.instance().start()
