from settings import conf_dic
import datetime
import fileinput


'''
var host = "";
var lista_de_revisores = ['','','','',''];
'''

host_string_js_py = 'var host = "http://%s:%s/";' % (conf_dic["domain"], conf_dic["appport"])
TODAY = datetime.date.today() 

revisor_list_string_js_py = conf_dic["list_revisors_var"] 


def check_host_string():
    # eduego/js/settings.js
    f = open("eduego/js/settings.js", "r")
    for i in f:
       if "var host" in i:
            if i == host_string_js_py:
                print "host OK!"
            else:
                print "settings.py = %s\nsettings.js = %s" % (host_string_js_py, i)
                print "--> correcting settings.js"
                for line in fileinput.input('eduego/js/settings.js', inplace=True):
                    print line.replace(i, host_string_js_py)


def check_revisors_list_string():
    # eduego/js/settings.js
    f = open("eduego/js/settings.js", "r")
    for i in f:
       if "var lista_de_revisores" in i:
            if i == revisor_list_string_js_py:
                print "revisors OK!"
            else:
                print "settings.py = %s\nsettings.js = %s" % (revisor_list_string_js_py, i)
                print "--> correcting settings.js"
                for line in fileinput.input('eduego/js/settings.js', inplace=True):
                    print line.replace(i, revisor_list_string_js_py)



def check_js_settings():
    check_host_string()
    check_revisors_list_string()



if __name__ == "__main__":
    check_js_settings()
