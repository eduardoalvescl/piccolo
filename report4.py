# -*- coding: utf-8 -*-
import MySQLdb
import datetime
import os
import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders
import os

def sendMail(to, subject, text, files=[]):
    assert type(to)==list
    assert type(files)==list
    fro = "Expediteur <expediteur@mail.com>"

    msg = MIMEMultipart()
    msg['From'] = "rebec"
    msg['pass'] = "ensaios12"
    msg['To'] = COMMASPACE.join(to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text) )

    for file in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"'
                       % os.path.basename(file))
        msg.attach(part)

    SMTP_SERVER = 'localhost'
    SMTP_PORT = 25
    smtp = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo
    smtp.login(msg['From'], msg["pass"])    
    smtp.sendmail(fro, to, msg.as_string() )
    smtp.close()
'''
EMAIL_HOST = 'localhost'
EMAIL_PORT = 25 # http://www.iana.org/assignments/port-numbers
### if set, used to authenticate with SMTP
EMAIL_HOST_USER = 'rebec'
EMAIL_HOST_PASSWORD = 'ensaios12'
EMAIL_USE_TLS = False
'''


def escreveLinha(outfile, linha):
    with open(outfile, 'a') as f:
        f.write("%s\n" % linha)


def gera_csv(row, outputfile):
	#linha = "%s;%s;%s;%s;%s;%s;%s;%s" % (row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7])
        linha = ""
        n_row = len(row)
        for i in range(0,n_row):
            linha = linha+"%s;"
        linha_report = (linha % row).strip(";")
        escreveLinha(outputfile,linha_report)
    
def report(report_query, report_header, report_name, report_destiny_list):
    hj = datetime.date.today()
    report_file = "%s_%s.csv" % (str(hj).replace("-",""), report_name)
    try:
       os.system("rm %s" % report_file)
    except:
       print "report_nao_existe"
    query = report_query
    db = MySQLdb.connect(host="localhost",
                         user="rebec",
                         passwd="ensaios@12",
                         db="opentrials_")
    cursor = db.cursor()
    cursor.execute(query)
    numrows = int(cursor.rowcount)

    header = report_header
    escreveLinha(report_file, header)

    for row in cursor.fetchall():
        gera_csv(row,report_file)



    sendMail(
        report_destiny_list,
        report_file ,"Em anexo %s.\n att. \nREBEC" % report_file,
        [report_file]
        )

def report_download(report_query, report_header, report_name):
    hj = datetime.date.today()
    report_file = "%s_%s.csv" % (str(hj).replace("-",""), report_name)
    try:
       os.system("rm %s" % report_file)
    except:
       print "report_nao_existe"
    query = report_query
    db = MySQLdb.connect(host="localhost",
                         user="rebec",
                         passwd="ensaios@12",
                         db="opentrials_")
    cursor = db.cursor()
    cursor.execute(query)
    numrows = int(cursor.rowcount)

    header = report_header
    escreveLinha(report_file, header)

    for row in cursor.fetchall():
        gera_csv(row,report_file)

    return report_file


report_nameP = 'relatorio_controle_revisao'
queryP = 'select rev.created,  rev.context,  (select username from auth_user where id = rev.creator_id) as revisor, sub.trial_id, concat("http://www.ensaiosclinicos.gov.br/rg/view/", sub.trial_id, "/")  as link_review, sub.status, (select username from auth_user where id = sub.creator_id) as registrante_user from reviewapp_remark rev, reviewapp_submission sub where  sub.id = rev.submission_id'
headerP = "data_revisao;campo_revisado;revisor;trial_id;link_review;status_estudo;username_registrante"
def gera_report():
    return report_download(queryP, headerP, report_nameP)





if __name__ == "__main__":
#    query = 'select  sub.id as sub_id, sub.creator_id as creator_id, sub.trial_id as trial_id, (select email from auth_user where id = sub.creator_id) as email, (select username from auth_user where id = sub.creator_id) as username, (select created from repository_clinicaltrial where id = sub.trial_id) created from  reviewapp_submission sub where  sub.status = "draft" and  sub.trial_id in (select id from repository_clinicaltrial where created < "2012-08-01");'
#    header = 'sub_id;creator_id;trial_id;creator_email;creator_username;created'
    report_name = 'relatorio_controle_revisao'
    mail_list = ["dtostes@gmail.com", "luiza1966@gmail.com", "josue.laguardia@icict.fiocruz.br", "mavelaralves@uol.com.br", "rebec.fiocruz@gmail.com", "eiras86@gmail.com", "gian.rebec@gmail.com", "limasvan@gmail.com", "eduardo.rebec@gmail.com"]
#    query = "select sub.id, (select id from reviewapp_attachment where submission_id = sub.id limit 1) as id_attach, sub.trial_id from reviewapp_submission sub"
#    query = 'select sub.id, (select id from reviewapp_attachment where submission_id = sub.id limit 1) as id_attach, concat("http://www.ensaiosclinicos.gov.br/rg/view/", sub.trial_id, "/") as revision_url from reviewapp_submission sub where sub.status = "pending";'
    query = 'select rev.created,  rev.context,  (select username from auth_user where id = rev.creator_id) as revisor, sub.trial_id, concat("http://www.ensaiosclinicos.gov.br/rg/view/", sub.trial_id, "/")  as link_review, sub.status, sub.updated, (select username from auth_user where id = sub.creator_id) as registrante_user, DATE_ADD(sub.updated, INTERVAL 45 DAY) from reviewapp_remark rev, reviewapp_submission sub where  sub.id = rev.submission_id;'
    header = "data_revisao;campo_revisado;revisor;trial_id;link_review;status_estudo;ultima_atualizacao;username_registrante;limite_para_resposta"
    report(query, header, report_name, mail_list) 
