OBJETIVO

O Registro Brasileiro de Ensaios Clínicos - REBEC  não gerava relatórios de performance e controle de revisão. Organizar e distribuir as revisões era um processo árduo e descentralizado. Não havia um painel de informações relevantes à tomada de decisão da coordenação. Este trabalho apresenta a construção do PICCOLO, sistema de código aberto capaz de gerar indicadores e relatórios de controle interno para  necessidades de informação relevantes a partir dos registros do Rebec.


METODOLOGIA

Escuta dos revisores, coordenação e dos usuários, o que originou três listas de demandas : (1) controle de curadoria dos ensaios submetidos; (2) resultados e visão geral da evolução da plataforma, em períodos específicos; (3) atualização de cadastro de usuários em qualquer ponto do processo de revisão. Em cada lista, foram estabelecidas hierarquias, percursos ou grupos de problemas, até compor uma visão geral unindo as três demandas  em um sistema satélite, consumidor de dados do Rebec. Dividiu-se o desenvolvimento em (a) Backend:  framework web Tornado em python com um um banco de dados Sqlite para persistir dados que não existiam no ReBEC. (b) Frontend: Framework Opensource Twitter BootStrap escrito em jquery e javascript; construído no Twitter por@mdo e @fat, o Bootstrap utiliza LESS CSS, é compilado via Node.js, e é gerenciado pelo GitHub.